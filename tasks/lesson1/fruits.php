<?php
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../.." ) ;
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

// task 1.1

$apple = new NewsProject\Fruits\Apple('зеленое', 'грени-смит', 'полированый');
$apple->print();

$banana = new NewsProject\Fruits\Banana('кислотно-желтый', 'вкус насыщенного калия', 'с оттенком папуаса');
$banana->print();

$mandarine = new NewsProject\Fruits\Mandarine('оранжевый', 'кисло-сладкий', 'с оттенком нового-года');
$mandarine->print();