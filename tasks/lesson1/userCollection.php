<?php
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../.." ) ;
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$user1 = new \NewsProject\User\User(18, 'Андрей', 'test@test.ru');
$user2 = new \NewsProject\User\User(3, 'Павел', 'test@test.ru');
$user3 = new \NewsProject\User\User(75, 'Игорь-цветок', 'test@test.ru');
$userCollection = new \NewsProject\User\UserCollection();

$userCollection->add($user1);
$userCollection->add($user2);
$userCollection->add($user3);

var_dump($userCollection->isExist('Игорь-цветок'));