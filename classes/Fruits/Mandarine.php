<?php


namespace NewsProject\Fruits;


class Mandarine
{
    public string $color;
    public string $taste;
    public string $shine;

    public function print() {
        echo "цвет - $this->color вкус - $this->taste блеск - $this->shine <br>" . PHP_EOL;
    }

    public function __construct(string $color, string $taste, string $shine) {
        $this->color = $color;
        $this->taste = $taste;
        $this->shine = $shine;
    }
}