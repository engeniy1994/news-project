<?php


namespace NewsProject\User;


class UserCollection
{
    public $users = array();

    public function add(User $user) {
        $this->users[] = $user;
    }

    public function isExist(string $name) {
        $isExist = false;

        foreach ($this->users as $user) {
            if($user->name == $name) {
                $isExist = true;
            }
        }
        return $isExist;
    }
}