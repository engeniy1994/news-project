<?php


namespace NewsProject\User;


class User
{
    public int $age;
    public string $name;
    public ?string $email;

    public function print() {
        echo "пользователь имя - $this->name <br> возраст - $this->age <br> email - $this->email <br>" . PHP_EOL;
    }

    public function sumAge(User $user) {
         echo $this->age + $user->age. "<br>" . PHP_EOL;
    }

    public function __construct(int $age, string $name, string $email = null) {
        $this->age = $age;
        $this->name = $name;
        $this->email = $email;
    }

}

